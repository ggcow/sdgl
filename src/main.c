#include <SDL2/SDL.h>
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <stdio.h>


#define WIDTH 1920
#define HEIGHT 1080

int main(int argc, char *argv[])
{
    SDL_Window *window;
    SDL_Event event;
    SDL_GLContext gl_context;

    SDL_Init(SDL_INIT_VIDEO);

	window = SDL_CreateWindow(
			"sdl-gl",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			WIDTH/2,
			HEIGHT/2,
			SDL_WINDOW_OPENGL | SDL_WINDOW_INPUT_GRABBED | SDL_WINDOW_RESIZABLE
    );
	
    gl_context = SDL_GL_CreateContext(window);

	SDL_GL_SetSwapInterval(1);

    // get version info
    const GLubyte *renderer = glGetString(GL_RENDERER); // get renderer string
    const GLubyte *version = glGetString(GL_VERSION); // version as a string
    printf("OpenGL version supported %s\n", version);
    printf("Renderer: %s\n", renderer);
    fflush(stdout);

    glViewport(0, 0, WIDTH/2, HEIGHT/2);
    glClearColor(0, 0, 0, 1);

    static const char * vertex_shader =
    "#version 330 core\n"
    "layout(location = 0) in vec2 vertex_position;\n"
    "void main() {\n"
    "    gl_Position = vec4(vertex_position, 1.0 );\n"
    "}\n";

    static const char * fragment_shader =
    "#version 330 core\n"
    "out vec4 color;\n"
    "void main() {\n"
    "    color = vec4(1);\n"
    "}\n";

    GLuint vs, fs, program;
    vs = glCreateShader(GL_VERTEX_SHADER);
    fs = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vs, 1, &vertex_shader, NULL);
    glShaderSource(fs, 1, &fragment_shader, NULL);

    glCompileShader(vs);
    glCompileShader(fs);

    program = glCreateProgram();
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    glDetachShader(program, vs);
    glDetachShader(program, fs);
    glDeleteShader(vs);
    glDeleteShader(fs);
    glUseProgram(program);

    float vertices[] = {
        -0.5f, -0.5f,
        0.5f, -0.5f,
        0.0f,  0.5f,
    };

    GLuint vao, vbo;
    glCreateVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glCreateBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(0);

    while (1) {
        SDL_PollEvent(&event);
        if (event.type == SDL_QUIT) {
            break;
        }
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                break;
            }
        }
        
        glClear(GL_COLOR_BUFFER_BIT);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        SDL_GL_SwapWindow(window);
    }
	
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
	SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    glDeleteProgram(program);
    SDL_Quit();
    return 0;
}